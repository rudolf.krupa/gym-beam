# Gym Beam

## Instalacia

`composer install`

## Spustenie

`php process-data.php`

## Poziadavky

PHP 8.1+

## Limity

Google neumoznuje cez api ziskat sentiment pre 
text pre viacero dokumentov sucasne. Tak som 
zapracoval moznost odosielat viacero requestov naraz cez curl.
V tejto chvili je to nastavene na 15 requestov (konstanta v triede).
Pri vacsom pocte moze nastat problem, lebo Google api ma 
limit na pocet requestov za minutu.

