<?php

use GymBeam\Product\OutputFormatter;
use GymBeam\Product\ParseCsv;
use GymBeam\Product\Process;
use GymBeam\Product\SentimentApi;

require __DIR__ . '/vendor/autoload.php';

try {
    OutputFormatter::header("Processing ...");

    $csvFilePath = __DIR__ . DIRECTORY_SEPARATOR . 'dataset-gymbeam-product-descriptions-eng.csv';
    $parseCsv = new ParseCsv($csvFilePath);

    $processText = new Process($parseCsv->getProducts(), new SentimentApi());

    OutputFormatter::header("Results:");

    OutputFormatter::header("Positive product descriptions");
    $positiveProductDescriptions = $processText->getPositiveDescriptions();
    OutputFormatter::formatProduct($positiveProductDescriptions);

    OutputFormatter::header("Negative product descriptions");
    $negativeProductDescriptions = $processText->getNegativeDescriptions();
    OutputFormatter::formatProduct($negativeProductDescriptions);

    // words sorted by number of occurrences
    $allWords = $processText->mostFrequentlyUsedWords();

    OutputFormatter::header("Most used words in descriptions");
    OutputFormatter::formatListOfWords($allWords);

} catch (Exception $e) {
    echo $e->getMessage();
    echo $e->getTraceAsString();
}



