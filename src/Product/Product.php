<?php

declare(strict_types=1);

namespace GymBeam\Product;

class Product
{
    private string $name;

    private string $description;

    private float $sentimentScore;

    public function __construct(string $name, string $description)
    {
        $this->name = $name;
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param float $sentimentScore
     */
    public function setSentimentScore(float $sentimentScore): void
    {
        $this->sentimentScore = $sentimentScore;
    }

    /**
     * @return float
     */
    public function getSentimentScore(): float
    {
        return $this->sentimentScore;
    }
}