<?php

declare(strict_types=1);

namespace GymBeam\Product\Exceptions;

use Exception;

class CsvFileNotFoundException extends Exception
{
}