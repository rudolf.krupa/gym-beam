<?php

declare(strict_types=1);

namespace GymBeam\Product;

use GymBeam\Product\Exceptions\SentimentApiException;

class SentimentApi
{
    private const API_URL = 'https://language.googleapis.com/v1/documents:analyzeSentiment?key=AIzaSyBQjeAfhHouaQsaAcDwL22b13hepQXrATs';

    private const TIMEOUT = 10;
    private const CONNECTION_TIMEOUT = 10;

    public function request(string $text): float
    {
        $request = $this->prepareRequest($text);
        $response = $this->curlRequest($request);
        return $this->parseResponse($response);
    }

    private function parseResponse(string $response): float
    {
        $responseDecoded = json_decode($response);
        if (!isset($responseDecoded->documentSentiment->score)) {
            throw new SentimentApiException("Sentiment api return wrong format of response: " . $response);
        }
        return (float)$responseDecoded->documentSentiment->score;;
    }

    private function prepareRequest(string $text): string
    {
        return json_encode([
            "document" => [
                "type" => "PLAIN_TEXT",
                "content" => $text,
            ],
            "encodingType" => "UTF8",
        ]);
    }

    private function curlRequest(string $requestJson): string
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, static::API_URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestJson);
        curl_setopt($ch, CURLOPT_TIMEOUT, static::TIMEOUT);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, static::CONNECTION_TIMEOUT);

        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json'
        ]);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new SentimentApiException("Curl error: " . curl_error($ch));
        }
        curl_close($ch);

        return $result;
    }

    public function multiRequests(array $batches): array
    {
        $multiInit = curl_multi_init();

        $requests = [];
        foreach ($batches as $key => $text) {

            $curlHandle = [
                'handler' => curl_init(static::API_URL)
            ];

            curl_setopt($curlHandle['handler'], CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curlHandle['handler'], CURLOPT_FOLLOWLOCATION, true);

            curl_setopt($curlHandle['handler'], CURLOPT_HTTPHEADER, [
                'Content-Type: application/json'
            ]);

            curl_setopt($curlHandle['handler'], CURLOPT_POST, true);
            curl_setopt($curlHandle['handler'], CURLOPT_POSTFIELDS, $this->prepareRequest($text));

            curl_setopt($curlHandle['handler'], CURLOPT_TIMEOUT, static::TIMEOUT);
            curl_setopt($curlHandle['handler'], CURLOPT_CONNECTTIMEOUT, static::CONNECTION_TIMEOUT);

            curl_setopt($curlHandle['handler'], CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curlHandle['handler'], CURLOPT_SSL_VERIFYPEER, false);

            curl_multi_add_handle($multiInit, $curlHandle['handler']);

            $requests[$key] = $curlHandle;
        }

        $stillRunning = false;
        do {
            curl_multi_exec($multiInit, $stillRunning);
        } while ($stillRunning);

        foreach ($requests as $key => $request) {
            curl_multi_remove_handle($multiInit, $request['handler']);

            if (curl_errno($request['handler'])) {
                throw new SentimentApiException("Curl error: " . curl_error($request['handler']));
            }

            $content = curl_multi_getcontent($request['handler']);
            $requests[$key]['content'] = $content;
            $requests[$key]['response'] = $this->parseResponse($content);
            $requests[$key]['http_code'] = curl_getinfo($request['handler'], CURLINFO_HTTP_CODE);

            curl_close($requests[$key]['handler']);
        }

        curl_multi_close($multiInit);
        return $requests;
    }
}