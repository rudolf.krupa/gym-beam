<?php

declare(strict_types=1);

namespace GymBeam\Product;

use GymBeam\Product\Exceptions\CsvFileNotFoundException;

class ParseCsv
{
    private const NBSP = ' ';

    /**
     * @var array []Product
     */
    private array $products;

    private bool $skipped = false;

    public function __construct(string $fileName, bool $skipFirst = true)
    {
        if (empty($fileName) || !file_exists($fileName)) {
            throw new CsvFileNotFoundException("File `$fileName` not found");
        }

        $handle = fopen($fileName, 'r');
        while (($rowData = fgetcsv($handle)) !== false) {

            if ($skipFirst && !$this->skipped) {
                $this->skipped = true;
                continue;
            }

            $description = str_replace(static::NBSP, ' ', strip_tags($rowData[1]));
            $this->products[] = new Product($rowData[0], $description);
        }
    }

    /**
     * @return array []Product
     */
    public function getProducts(): array
    {
        return $this->products;
    }
}