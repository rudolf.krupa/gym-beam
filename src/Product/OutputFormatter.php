<?php

declare(strict_types=1);

namespace GymBeam\Product;

class OutputFormatter
{
    public static function header(string $header): void
    {
        echo $header . PHP_EOL . PHP_EOL;
    }

    public static function formatProduct(array $products): void
    {
        foreach ($products as $product) {
            echo "Sentiment: " . $product->getSentimentScore() . PHP_EOL;
            echo $product->getName() . PHP_EOL;
            echo $product->getDescription() . PHP_EOL . PHP_EOL;
        }
        echo PHP_EOL;
    }

    public static function formatListOfWords(array $words): void
    {
        foreach ($words as $word => $occurrences) {
            echo str_pad($word, 8, " ") . " " . $occurrences . PHP_EOL;
        }
        echo PHP_EOL;
    }
}