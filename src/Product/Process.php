<?php

declare(strict_types=1);

namespace GymBeam\Product;

use InvalidArgumentException;

class Process
{
    private const MOST_FREQUENTLY_USED_WORDS_LIMIT = 10;

    private const LIMIT_FOR_PRODUCT_DESCRIPTIONS = 5;

    private const REQUEST_BATCH_LIMIT = 15;

    public const ASC = 'asc';
    public const DESC = 'desc';

    private const SORT_OPTIONS = [
        Process::ASC,
        Process::DESC,
    ];

    private const IGNORE_CHARACTERS = [
        ',',
        '.',
        '!',
        '"',
        '(',
        ')',
        ':',
        '%',
        '-',
        '€',
    ];

    private const IGNORE_WORDS = [
        'and',
        'but',
        'or',
        'of',
        'nor',
        'yet',
        'so',
        'the',
        'a',
        'an',
        'to',
        'in',
        'is',
        'by',
        'for',
        'f',
//        'also',
//        'your',
//        'it',
//        'which',
//        'that',
//        'as',
//        'are',
//        'you',
//        'can',
//        'from',
//        'will',
//        'its',
//        'has',
//        'made',
//        'not',
//        'contains',
    ];
    /**
     * @var array
     */
    private array $products;

    private array $positiveProducts;

    private array $negativeProducts;

    private string $allDescriptions = '';

    public function __construct(array $products, SentimentApi $textSentimentApi, bool $batchRequests = true)
    {
        $this->products = $products;

        if ($batchRequests) {
            $batches = array_chunk($this->products, static::REQUEST_BATCH_LIMIT, true);
            foreach ($batches as $batch) {

                $productsDescriptions = [];
                foreach ($batch as $productKey => $product) {
                    $productsDescriptions[$productKey] = $product->getDescription();
                }
                $responses = $textSentimentApi->multiRequests($productsDescriptions);

                foreach ($batch as $productKey => $product) {
                    if (isset($responses[$productKey])) {
                        $product->setSentimentScore($responses[$productKey]['response']);
                    }
                }
            }
        } else {
            foreach ($this->products as $product) {
                /** @var Product $product */
                $textSentiment = $textSentimentApi->request($product->getDescription());
                $product->setSentimentScore($textSentiment);
            }
        }

        // merge all descriptions & sort by sentiment
        foreach ($this->products as $product) {
            $this->allDescriptions .= $this->cleanText($product->getDescription());
            $this->sortOut($product);
        }
    }

    public function getPositiveDescriptions(int $limit = Process::LIMIT_FOR_PRODUCT_DESCRIPTIONS): array
    {
        $positiveProducts = $this->sortProductsBySentiment($this->positiveProducts, static::DESC);
        return array_slice($positiveProducts, 0, $limit, true);
    }

    public function getNegativeDescriptions(int $limit = Process::LIMIT_FOR_PRODUCT_DESCRIPTIONS): array
    {
        $negativeProducts = $this->sortProductsBySentiment($this->negativeProducts, static::ASC);
        return array_slice($negativeProducts, 0, $limit, true);
    }

    private function sortOut(Product $product) {
        if ($product->getSentimentScore() > 0) {
            $this->positiveProducts[] = $product;
        } else if ($product->getSentimentScore() < 0) {
            $this->negativeProducts[] = $product;
        }
    }

    private function sortProductsBySentiment(array $products, string $sort = Process::ASC): array
    {
        if (!in_array($sort, static::SORT_OPTIONS)) {
            throw new InvalidArgumentException($sort . ' is not valid option for sort');
        }

        usort($products, function ($a, $b) use ($sort) {
            if ($a->getSentimentScore() === $b->getSentimentScore()) {
                return 0;
            }
            switch ($sort) {
                case static::ASC;
                    $result = ($a->getSentimentScore() < $b->getSentimentScore()) ? -1 : 1;
                    break;
                case static::DESC;
                    $result = ($a->getSentimentScore() > $b->getSentimentScore()) ? -1 : 1;
                    break;
            }
            return $result;
        });
        return $products;
    }

    // words sorted by number of occurrences
    public function mostFrequentlyUsedWords(int $limit = Process::MOST_FREQUENTLY_USED_WORDS_LIMIT): array
    {
        $words = explode(' ', $this->allDescriptions);

        foreach ($words as $wordKey => $word) {
            if (empty($word) || is_numeric($word)) {
                unset($words[$wordKey]);
            }
        }

        $listOfWords = array_count_values($words);
        arsort($listOfWords);
        return array_slice($listOfWords, 0, $limit, true);
    }

    private function cleanText(string $input): string
    {
        $input = str_replace(static::IGNORE_CHARACTERS, '', $input);

        foreach (static::IGNORE_WORDS as $ignoreWord) {
            $input = preg_replace('/\b' . $ignoreWord . '\b/i', ' ', $input);
        }

        $input = preg_replace('!\s+!', ' ', $input);
        return strtolower($input);
    }
}